package main

import (
	"crypto/md5"
	"fmt"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v2"
)

const (
	identifierTypeNpm      report.IdentifierType = "npm"
	identifierTypeRetireJs report.IdentifierType = "retire.js"

	urlPrefixNpm       = "www.npmjs.com/advisories/"
	urlPrefixHackerone = "hackerone.com/reports/"

	namePrefixNpm       = "NPM-"
	namePrefixHackerone = "HACKERONE-"
	namePrefixRetireJs  = "RETIRE-JS-"
)

// parseIdentifierURL turns a URL like http://osvdb.org/show/osvdb/94561 into an Identifier.
func parseIdentifierURL(url string) report.Identifier {

	// supported URLs (no scheme) along with builder functions
	var handlers = []struct {
		URL     string
		Builder func(idStr string) report.Identifier
	}{
		{
			URL:     urlPrefixNpm,
			Builder: npmIdentifier,
		},
		{
			URL:     "nodesecurity.io/advisories/",
			Builder: npmIdentifier,
		},
		{
			URL:     urlPrefixHackerone,
			Builder: hackeroneIdentifier,
		},
		{
			URL: "osvdb.org/show/osvdb/",
			Builder: func(idStr string) report.Identifier {
				return report.OSVDBIdentifier("OSVDB-" + idStr)
			},
		},
	}

	// pattern matching
	noScheme := strings.TrimPrefix(strings.TrimPrefix(url, "http://"), "https://")
	for _, handler := range handlers {
		if strings.HasPrefix(noScheme, handler.URL) {
			idStr := strings.TrimPrefix(noScheme, handler.URL)
			return handler.Builder(idStr)
		}
	}

	// else turn URL into an identifier
	return retireJsIdentifier(url)
}

func npmIdentifier(idStr string) report.Identifier {
	return report.Identifier{
		Type:  identifierTypeNpm,
		Name:  namePrefixNpm + idStr,
		Value: idStr,
		URL:   "https://" + urlPrefixNpm + idStr,
	}
}

func hackeroneIdentifier(idStr string) report.Identifier {
	return report.H1Identifier(namePrefixHackerone + idStr)
}

func retireJsIdentifier(url string) report.Identifier {
	idStr := fmt.Sprintf("%x", md5.Sum([]byte(url)))
	return report.Identifier{
		Type:  identifierTypeRetireJs,
		Name:  namePrefixRetireJs + idStr,
		Value: idStr,
		URL:   url,
	}
}
