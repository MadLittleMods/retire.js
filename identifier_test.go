package main

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v2"
)

func TestParseIdentifierURL(t *testing.T) {
	var tcs = []struct {
		URL  string
		want report.Identifier
	}{
		{
			URL: "https://hackerone.com/reports/350401",
			want: report.Identifier{
				Type:  "hackerone",
				Name:  "HACKERONE-350401",
				Value: "350401",
				URL:   "https://hackerone.com/reports/350401",
			},
		},
		{
			URL: "https://www.npmjs.com/advisories/50",
			want: report.Identifier{
				Type:  "npm",
				Name:  "NPM-50",
				Value: "50",
				URL:   "https://www.npmjs.com/advisories/50",
			},
		},
		{
			URL: "https://nodesecurity.io/advisories/51",
			want: report.Identifier{
				Type:  "npm",
				Name:  "NPM-51",
				Value: "51",
				URL:   "https://www.npmjs.com/advisories/51",
			},
		},
		{
			URL:  "http://osvdb.org/show/osvdb/94561",
			want: report.OSVDBIdentifier("OSVDB-94561"),
		},
		{
			URL: "https://bugs.jquery.com/ticket/11974",
			want: report.Identifier{
				Type:  "retire.js",
				Name:  "RETIRE-JS-48ceeb5bdae52231e03df9e98e72532e",
				Value: "48ceeb5bdae52231e03df9e98e72532e",
				URL:   "https://bugs.jquery.com/ticket/11974",
			},
		},
	}

	for _, tc := range tcs {
		name := string(tc.want.Type)
		t.Run(name, func(t *testing.T) {
			got := parseIdentifierURL(tc.URL)
			require.Equal(t, tc.want, got)
		})
	}
}
